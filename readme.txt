Подключение к базе данных осуществляется через JNDI

пример файла context.xml на сервере Tomcat

<Context>
	......

	<Resource name="NAME" auth="Container" type="javax.sql.DataSource" 
		maxActive="100" maxIdle="30" maxWait="10000" username="LOGIN" password="PASSWORD" 
		driverClassName="org.gjt.mm.mysql.Driver" url="PATH_TO_DB" 
		encoding="UTF-8"/> 
	.......
</Context>


Например:
	NAME - "jdbc/PhoneBookDB"
	LOGIN - "root"
	PASSWORD - "password"
	PATH_TO_DB - "jdbc:mysql://127.0.0.1:3306/phonebook?characterEncoding=utf-8"
