package com.lardi_trans.test.model;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class JDBCConnection {
	
	// запрещаем прямое создание объекта
	private JDBCConnection() {}
	
	private static volatile Connection connection; 
	
	public static Connection getInstance() {
		
		if (connection == null) {
			
			synchronized (JDBCConnection.class) {
				
				if (connection == null) {
					
					try {
						
						InitialContext initialContext = new InitialContext();
			            DataSource dataSource = (DataSource)initialContext.lookup("java:comp/env/jdbc/PhoneBookDB");
			            connection = dataSource.getConnection();
			 
					} catch (SQLException e) {
			 
						System.out.println("Connection Failed! Check output console");
						e.printStackTrace();
						return null;
			 
					} catch (NamingException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
		
		return connection;
	}
}
