package com.lardi_trans.test.model.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lardi_trans.test.model.JDBCConnection;
import com.lardi_trans.test.model.dao.Dao;
import com.lardi_trans.test.model.entity.Entry;

public class EntryDao implements Dao {

	private Connection connection;
	
	private final String INSERT = "INSERT INTO entry(last_name, first_name, patronymic, mobile_phone, home_phone, address, email) " +
			"VALUE(?, ?, ?, ?, ?, ?, ?)";
	private final String UPDATE = "UPDATE entry SET last_name = ?, first_name = ?, patronymic = ?, mobile_phone = ?, " +
			"home_phone = ?, address = ?, email = ? WHERE id = ?";
	private final String DELETE = "DELETE FROM entry WHERE id = ?";
	private final String SELECT_ALL = "SELECT * FROM entry";
	
	
	/**
	 * Сохранить новую запись
	 * 
	 * @param entry объект создаваемой записи
	 */
	public boolean save(Entry entry) {
		
		connection = JDBCConnection.getInstance();

		PreparedStatement preparedStatement = null;

		try {
			
			preparedStatement = connection.prepareStatement(INSERT);
			preparedStatement.setString(1, entry.getLastName());
			preparedStatement.setString(2, entry.getFirstName());
			preparedStatement.setString(3, entry.getPatronymic());
			preparedStatement.setString(4, entry.getMobilePhone());
			preparedStatement.setString(5, entry.getHomePhone());
			preparedStatement.setString(6, entry.getAddress());
			preparedStatement.setString(7, entry.getEmail());
			
			boolean result = preparedStatement.execute();
			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Обновление записи
	 * 
	 * @param entry объект обновляемой записи
	 */
	public boolean update(Entry entry) {
		
		connection = JDBCConnection.getInstance();

		PreparedStatement preparedStatement = null;

		try {
			
			preparedStatement = connection.prepareStatement(UPDATE);
			preparedStatement.setString(1, entry.getLastName());
			preparedStatement.setString(2, entry.getFirstName());
			preparedStatement.setString(3, entry.getPatronymic());
			preparedStatement.setString(4, entry.getMobilePhone());
			preparedStatement.setString(5, entry.getHomePhone());
			preparedStatement.setString(6, entry.getAddress());
			preparedStatement.setString(7, entry.getEmail());
			preparedStatement.setLong(8, entry.getId());
			
			boolean result = preparedStatement.execute();
			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Удаление записи
	 * 
	 * @param id id-удаляемой записи
	 */
	public boolean delete(Long id) {
		
		connection = JDBCConnection.getInstance();

		PreparedStatement preparedStatement = null;

		try {
			
			preparedStatement = connection.prepareStatement(DELETE);
			preparedStatement.setLong(1, id);
			boolean result = preparedStatement.execute();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * @return множество элементов Entry
	 */
	public List<Entry> getAllEntry() {
	
		connection = JDBCConnection.getInstance();
		
		List<Entry> entries = new ArrayList<Entry>();

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null; 
		
		try {
			
			preparedStatement = connection.prepareStatement(SELECT_ALL);
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {

				Entry entry = new Entry();
				
				entry.setId(resultSet.getLong("id"));
				entry.setLastName(resultSet.getString("last_name"));
				entry.setFirstName(resultSet.getString("first_name"));
				entry.setPatronymic(resultSet.getString("patronymic"));
				entry.setMobilePhone(resultSet.getString("mobile_phone"));
				entry.setHomePhone(resultSet.getString("home_phone"));
				entry.setAddress(resultSet.getString("address"));
				entry.setEmail(resultSet.getString("email"));
				
				entries.add(entry);
			}
			
			return entries;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
