package com.lardi_trans.test.model.dao;

import java.util.List;

import com.lardi_trans.test.model.entity.Entry;

/**
 * Предназначен для комманд БД
 * 
 * @author mark
 */
public interface EntryDao {

	/**
	 * Сохранить новую запись в БД
	 * 
	 * @param entry объект записи
	 */
	public Long save(Entry entry);
	
	/**
	 * Обновить запись
	 * 
	 * @param entry объект записи
	 */
	public boolean update(Entry entry);

	/**
	 * Удалить запись
	 * 
	 * @param id id-записи
	 */
    public boolean delete(Long id);
    
    /**
     * Получить список записей
     * 
     * @return список всех записей
     */
    public List<Entry> getAllEntries();

    /**
     * Получить запись
     * 
     * @param id
     * 
     * @return объект записи
     */
	public Entry getEntry(Long id);
}
