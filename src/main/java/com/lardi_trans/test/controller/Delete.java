package com.lardi_trans.test.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lardi_trans.test.service.EntityService;
import com.lardi_trans.test.service.action.EntryService;

public class Delete extends HttpServlet {

	
	static int i;
	private static final long serialVersionUID = -5861252447298822216L;

	private EntityService entryService;

	public void init() throws ServletException {
		entryService = new EntryService();
	}

	protected void doPost(HttpServletRequest request,
		
		HttpServletResponse response) throws ServletException, IOException {

		i++;
		
		//System.out.println(i);
		
		request.setCharacterEncoding("UTF-8");
		
		String checkboxesArray = request.getParameter("id");

		if (checkboxesArray != null) {
		
			String[] checkboxes = checkboxesArray.split(",");
			
			if (checkboxes != null && checkboxes.length > 0) {

				for (String checkbox : checkboxes) {
					
					if (CacheEntries.getInstance().containsKey(checkbox)) {
						entryService.delete(Long.parseLong(checkbox));
					}
				}
				
				PrintWriter out = response.getWriter();
				
				Gson gson = new Gson();
				
				out.println(gson.toJson(checkboxes));
			}
		}
	}
}
