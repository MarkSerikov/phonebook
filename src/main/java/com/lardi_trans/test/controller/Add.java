package com.lardi_trans.test.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.google.gson.Gson;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.EntityService;
import com.lardi_trans.test.service.action.EntryService;

public class Add extends HttpServlet {

	private static final long serialVersionUID = 4691593479118083051L;
	private EntityService entryService;

	public void init() throws ServletException {

		entryService = new EntryService();

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse responce) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		responce.setCharacterEncoding("UTF-8");

		String jsonStringParameter = request.getParameter("request");

		JSONObject jsonObject = null;
		
		String lastName = null, firstName = null, patronymic = null, mobilePhone = null,
				homePhone = null, address = null, email = null;

		PrintWriter out = responce.getWriter();
		Gson gson = new Gson();

		try {

			jsonObject = JSONObject.fromObject(jsonStringParameter);
			Object lastNameObject = jsonObject.get("lastName");
			Object firstNameObject = jsonObject.get("firstName");
			Object patronymicObject = jsonObject.get("patronymic");
			Object mobilePhoneObject = jsonObject.get("mobilePhone");
			Object homePhoneObject = jsonObject.get("homePhone");
			Object addressObject = jsonObject.get("address");
			Object emailObject = jsonObject.get("email");
			
			if (lastNameObject != null && firstNameObject != null && patronymicObject != null && mobilePhoneObject != null) {
				
				lastName = (String) lastNameObject;
				firstName = (String) firstNameObject;
				patronymic = (String) patronymicObject;
				mobilePhone = (String) mobilePhoneObject;
				
				if (homePhoneObject != null) {
					homePhone = (String) homePhoneObject;
				}
				if (addressObject != null) {
					address = (String) addressObject;
				}
				if (emailObject != null) {
					email = (String) emailObject;
				}
				
				if (!lastName.isEmpty() && !firstName.isEmpty() && !patronymic.isEmpty() && !mobilePhone.isEmpty()) {

					Entry entry = new Entry();
					entry.setLastName(lastName);
					entry.setFirstName(firstName);
					entry.setPatronymic(patronymic);
					entry.setMobilePhone(mobilePhone);
					entry.setHomePhone(homePhone);
					entry.setAddress(address);
					entry.setEmail(email);
					
					Long lastId = entryService.save(entry);
					
					entry.setId(lastId);
					
					CacheEntries.getInstance().put(entry.getId(), entry);

					out.println(gson.toJson(entry));

				}
			} 

		} catch (JSONException e) {
			
			e.printStackTrace();
		}
	}
}
