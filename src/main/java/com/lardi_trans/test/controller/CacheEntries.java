package com.lardi_trans.test.controller;

import java.util.HashMap;
import java.util.Map;

import com.lardi_trans.test.model.entity.Entry;

public class CacheEntries {
	
	private static volatile Map<Long, Entry> cacheEntry;
	
	public static Map<Long, Entry> getInstance() {
		
		if (cacheEntry == null) {
			synchronized (CacheEntries.class) {
				if (cacheEntry == null) {
					cacheEntry = new HashMap<Long, Entry>();
				}
			}
		}
		return cacheEntry;
	}
	
	private CacheEntries() {}
	
}
