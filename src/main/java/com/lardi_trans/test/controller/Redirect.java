package com.lardi_trans.test.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lardi_trans.test.service.EntityService;
import com.lardi_trans.test.service.action.EntryService;

public class Redirect extends HttpServlet {

	private static final long serialVersionUID = 4691593479118083051L;
	
	private final String ADD_PAGE = "/phonebook/add";
	private final String INDEX_PAGE = "/phonebook/index";
	private EntityService entryService;
	
	public void init() throws ServletException {
		entryService = new EntryService();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse responce)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		responce.sendRedirect(INDEX_PAGE);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse responce)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String button = request.getParameter("button");
		
		if ("create".equals(button))  {

			responce.sendRedirect(ADD_PAGE);
		
		} else if ("delete".equals(button)) { 
			
			String[] checkboxes = request.getParameterValues("check");
			
			if (checkboxes != null) {
				
				for (String checkbox : checkboxes) {
					
					entryService.delete(Long.parseLong(checkbox));
				}
			}
			
			responce.sendRedirect(INDEX_PAGE);
		
		} else if (button == null) {
			
			responce.sendRedirect(INDEX_PAGE);
		} 
	}
}
