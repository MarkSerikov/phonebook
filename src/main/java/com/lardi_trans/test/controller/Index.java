package com.lardi_trans.test.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.EntityService;
import com.lardi_trans.test.service.action.EntryService;

public class Index extends HttpServlet {

	/**
	 * Generated serial UID
	 */
	private static final long serialVersionUID = 9077299148961464706L;
	
	private EntityService entryService;
	private final String INDEX = "view/index.jsp";
	
	public void init() throws ServletException {
		
		entryService = new EntryService();

		List<Entry> entries = entryService.getAllEntry();

		Map<Long, Entry> cacheEntries = CacheEntries.getInstance();
		
		Iterator<Entry> iteratorEntries = entries.iterator();
		
		Entry entry = null;
		
		while (iteratorEntries.hasNext()) {
		
			entry = iteratorEntries.next();
			cacheEntries.put(entry.getId(), entry);
	
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse responce)
			throws ServletException, IOException {
	
		Map<Long, Entry> cacheEntry = CacheEntries.getInstance();
	
		Collection<Entry> entries = cacheEntry.values();
		
		request.setAttribute("entries", entries);
		
		request.setCharacterEncoding("UTF-8");

		request.getRequestDispatcher(INDEX).forward(request, responce);

	}
}
