package com.lardi_trans.test.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.google.gson.Gson;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.EntityService;
import com.lardi_trans.test.service.action.EntryService;

public class Edit extends HttpServlet {

	/**
	 * Generated serial UID
	 */
	private static final long serialVersionUID = -5384850420053712036L;

	private EntityService entryService;

	public void init() throws ServletException {
		entryService = new EntryService();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		
		Gson gson = new Gson();

		String idString = request.getParameter("id");

		try {
			
			Long id = Long.parseLong(idString);
			
			if (CacheEntries.getInstance().containsKey(id)) {

				Entry entry = entryService.getEntry(id);

				if (entry != null) {
					out.println(gson.toJson(entry));
				}
			}
					
		} catch (NumberFormatException e) {
			
			e.printStackTrace();
			
		}
		
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		response.setCharacterEncoding("UTF-8");

		String jsonStringParameter = request.getParameter("request");

		JSONObject jsonObject = null;
		
		String lastName = null, firstName = null, patronymic = null, mobilePhone = null,
				homePhone = null, address = null, email = null, idString = null;
		Long id = 0L;

		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {

			jsonObject = JSONObject.fromObject(jsonStringParameter);
			
			System.out.println(jsonObject);
			
			Object idObject = jsonObject.get("id");
			Object lastNameObject = jsonObject.get("lastName");
			Object firstNameObject = jsonObject.get("firstName");
			Object patronymicObject = jsonObject.get("patronymic");
			Object mobilePhoneObject = jsonObject.get("mobilePhone");
			Object homePhoneObject = jsonObject.get("homePhone");
			Object addressObject = jsonObject.get("address");
			Object emailObject = jsonObject.get("email");
			
			if (idObject != null && lastNameObject != null && firstNameObject != null 
					&& patronymicObject != null && mobilePhoneObject != null) {
				
				idString = (String) idObject;
				id = Long.parseLong(idString);
				lastName = (String) lastNameObject;
				firstName = (String) firstNameObject;
				patronymic = (String) patronymicObject;
				mobilePhone = (String) mobilePhoneObject;
				
				if (homePhoneObject != null) {
					homePhone = (String) homePhoneObject;
				}
				if (addressObject != null) {
					address = (String) addressObject;
				}
				if (emailObject != null) {
					email = (String) emailObject;
				}
				
				if (id.longValue() != 0 && !lastName.isEmpty() && !firstName.isEmpty() 
						&& !patronymic.isEmpty() && !mobilePhone.isEmpty()) {

					Entry entry = new Entry();
					entry.setId(id);
					entry.setLastName(lastName);
					entry.setFirstName(firstName);
					entry.setPatronymic(patronymic);
					entry.setMobilePhone(mobilePhone);
					entry.setHomePhone(homePhone);
					entry.setAddress(address);
					entry.setEmail(email);
					
					boolean flag = entryService.update(entry);
					
					if (flag) {
						Entry entryLast = entryService.getEntry(entry.getId());
						
						out.println(gson.toJson(entryLast));
					}
				}
			} 

		} catch (JSONException e) {
			
			e.printStackTrace();
		}
	}
}
