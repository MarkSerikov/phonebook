package com.lardi_trans.test.service.action;

import java.util.List;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.dao.mysql.EntryDaoImpl;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.EntityService;

public class EntryService implements EntityService {

	private EntryDao entryDao;
	
	public EntryDao getEntryDao() {
		return entryDao;
	}

	public void setEntryDao(EntryDao entryDao) {
		this.entryDao = entryDao;
	}

	public EntryService() {
		entryDao = new EntryDaoImpl();
	}
	
	/**
	 * Добавление новой записи
	 * 
	 * @param entry объект записи
	 */
	public Long save(Entry entry) {
		
		if (entry != null && entry.getLastName() != null && !entry.getLastName().isEmpty()
				&& entry.getFirstName() != null && !entry.getFirstName().isEmpty()
				&& entry.getPatronymic() != null && !entry.getPatronymic().isEmpty() 
				&& entry.getMobilePhone() != null && !entry.getMobilePhone().isEmpty()) {
			
			return entryDao.save(entry);
		}
		
		return null;
		
		
	}

	/**
	 * Обновление существующей записи
	 * 
	 * @param entry объект записи
	 */
	public boolean update(Entry entry) {
		
		if (entry != null && entry.getId() != null
				&& entry.getLastName() != null && !entry.getLastName().isEmpty()
				&& entry.getFirstName() != null && !entry.getFirstName().isEmpty()
				&& entry.getPatronymic() != null && !entry.getPatronymic().isEmpty() 
				&& entry.getMobilePhone() != null && !entry.getMobilePhone().isEmpty()) {
		
			return entryDao.update(entry);
		}
		
		return false;
	}

	/**
	 * Удалене записи
	 * 
	 * @param id id-удаляемой записи
	 */
	public boolean delete(Long id) {
		
		return entryDao.delete(id);
	}

	public List<Entry> getAllEntry() {
		return entryDao.getAllEntries();
	}

	public Entry getEntry(Long id) {
		return entryDao.getEntry(id);
	}
}
