package com.lardi_trans.test.service;

import java.util.List;

import com.lardi_trans.test.model.entity.Entry;

public interface EntityService {
	
	/**
	 * Сохранить новую запись в БД
	 * 
	 * @param entry объект записи
	 */
	public Long save(Entry entry);
	
	/**
	 * Обновить запись
	 * 
	 * @param entry объект записи
	 */
	public boolean update(Entry entry);

	/**
	 * Удалить запись
	 * 
	 * @param id id-записи
	 */
    public boolean delete(Long id);
    
    /**
     * @return множество всех записей
     */
    public List<Entry> getAllEntry();

    /**
     * Возвращает запись по id
     * 
     * @param id id-записи
     * 
     * @return объект записи
     */
	public Entry getEntry(Long id);
    
}
