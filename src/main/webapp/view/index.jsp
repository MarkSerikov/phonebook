<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="entry" class="com.lardi_trans.test.model.entity.Entry"
	scope="request"></jsp:useBean>

<html>
<head>
<link type="text/css" href="view/css/bootstrap.css" rel="Stylesheet" />
<link type="text/css" href="css/jquery-ui-1.10.1.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.js"></script>

<style type="text/css">

	#newEntry, #newEntryButton, #find, #cancel, #fixed-search {
		display: none;
		
	}
	#newEntryButton {
		text-align: center; 
	}
	#newEntry {
		border: 1px solid red;
	}
	#myForm {
		margin-left: 10px;
	}
	#hideLine {
		width: 5%;
        height: 65px;
        border: 1px solid red;
        position: fixed;
        bottom: 0px;
        left: 95%;
        background-color: #cccccc;
	}
	#fixed-search {
		width: 100%;
        height: 50px;
        border: 1px solid blue;
        position: fixed;
        bottom: 65px;
        background-color: #ccffff;
	}
	#fixed-div {
         width: 95%;
         height: 65px;
         border: 1px solid red;
         position: fixed;
         bottom: 0px;
         background-color: #cccccc;
    }
	#container {
		height: 100%;
        position: relative;
	}
      
</style>

</head>
<body onload="addIntoHashTable()">

<div id="container">

	${counter}
	<div id="messenger"> </div>
	<div id="content">
		<table class="table table-striped" id="myTable">
		
		<c:if test="${empty entries}">
			Записей еще нет
		</c:if>
			<c:if test="${not empty entries}">
				<thead> 
					<tr onclick="showAll();">
						<th><input type="checkbox" id="selall"></th>
						<th>Фамилия</th>
						<th>Имя</th>
						<th>Отчество</th>
						<th>Мобильный телефон</th>
						<th>Домашний телефон</th>
						<th>Адрес</th>
						<th>@-mail</th>
					</tr>
				</thead>	
				<tbody>
					<c:forEach items="${entries}" var="entry">
					
						<tr id="info" class="${entry.id}" >
							<td>
								<input type="checkbox" name="check" value="${entry.id}" id="ch" class="mc">
							</td>
							<td class="lastName">${entry.lastName}</td>
							<td class="firstName">${entry.firstName}</td>
							<td class="patronymic">${entry.patronymic}</td>
							<td class="mobilePhone">${entry.mobilePhone}</td>
							<td class="homePhone">${entry.homePhone}</td>
							<td class="address">${entry.address}</td>
							<td class="email">${entry.email}</td>
						</tr>
					
					</c:forEach>
				</tbody>
			</c:if>
		
		</table>
		
		</div>
		
		<div id="fixed-search">
			<table>
				<tr id="find">
					<td></td>
					<td><input type="text" id="lastName" /></td>
					<td><input type="text" id="firstName" /></td>
					<td><input type="text" id="patronymic" /></td>
					<td><input type="text" id="mobilePhone" /></td>
					<td><input type="text" id="homePhone" /></td>
					<td><input type="text" id="address"/></td>
					<td><input type="text" id="email"/></td>
				</tr>
			</table>
		</div>
		<div id="fixed-div">
			<form id="addNewEntryForm">
				<table>
					<tr id="newEntry">
						<td><input type="hidden" name="id" /></td>
						<td><input type="text" name="lastName" /></td>
						<td><input type="text" name="firstName" /></td>
						<td><input type="text" name="patronymic" /></td>
						<td><input type="text" name="mobilePhone" /></td>
						<td><input type="text" name="homePhone" /></td>
						<td><input type="text" name="address"/></td>
						<td><input type="text" name="email"/></td>
					</tr>
					<tr id="newEntryButton">
						<td colspan="8">
							<input type="button" onclick="saveNewEntry();" value="сохранить" class="btn btn-success" />
							<input type="button" onclick="hidenNewEntry();" value="отмена"class="btn btn-info"/>
						</td>
					</tr>
				</table>
			</form>

			<form id="myForm">
				<input type="button" name="button" value="create" onclick="create();" class="btn btn-primary" />
				<input type="submit" name="button" value="delete" onclick="deleteEntries();" class="btn btn-danger"/>
				<input type="button" name="button" value="search" onclick="search();" class="btn btn-warning" id="srch"/>
				<input type="button" name="button" value="cancel" onclick="cancelSearch();" class="btn btn-info" id="cancel"/>
			</form>
		
		</div>
		<div id="hideLine"></div>	
</div>

	<script type="text/javascript">

		$("body").on("click", "form#myForm", (function($this) {
			$("#fixed-div").hide();
		}));
		$("body").on("click", "#hideLine", (function($this) {
			$("#fixed-div").show();
		}));
	
	
		function cancelSearch() {
			$("#find").hide();
			$("#srch").show();
			$("#cancel").hide();
			$("#fixed-search").hide();
			$("tr#info").show();
		}
		
		function search() {
			$("#find").show("slow");
			$("#srch").hide();
			$("#cancel").show();
			$("#fixed-search").show();
		}

		function showAll() {
			$("tr#info").show();
		}
		
		var myHash = {};
		
		function addIntoHashTable() {
	
			$("table#myTable").tablesorter( {
				sortList: [[1,0]],
				headers: { 
					0: {
						sorter: false 
					}
				}
			});
			
			$("tr#info").each(function() {

				var atr = {};
				atr["lastName"] = $(this).find(".lastName").text();
				atr["firstName"] = $(this).find(".firstName").text();
				atr["patronymic"] = $(this).find(".patronymic").text();
				atr["mobilePhone"] = $(this).find(".mobilePhone").text();
				atr["homePhone"] = $(this).find(".homePhone").text();
				atr["address"] = $(this).find(".address").text();
				atr["email"] = $(this).find(".email").text();
				
				myHash[($(this).attr("class"))] = atr;
				
			});
		};
		
		function findEntries() {
			$("tr#info").hide();
		}
		
		$("#lastName, #firstName, #patronymic, #mobilePhone, #homePhone, #address, #email").on("keyup", function() {
			
			findEntries();
			
			for(var item in myHash) {
				for( var subItem in myHash[item]) {
					if((myHash[item][subItem].search($(this).val()) != -1) && subItem == $(this).attr("id") && $(this).val() != "") {
						$("."+item).show();
					}
					if($(this).val() == "") {
						showAll();
					}
				}
			}
			
		});

		function hidenNewEntry() {
			$("#newEntry").hide("slow");
			$("#newEntryButton").hide("slow");
			$("#messenger").hide("slow");
		}
		
		function saveNewEntry() {
			
			validate();
			
			var map = new Object();
			var mc = $(':input[type=text]');
			mc.each(function() {
				map[$(this).attr("name")] = $(this).val();
			});
			
			//var myobj = { lastName: 'Serikov', firstName: 'Mark', patronymic: 'Evg', mobilePhone: '099043'};

			$.ajax({
				type: "POST",
				url: "/phonebook/add",
				data: "request="+JSON.stringify(map),
				cache: false,
				dataType: "json",
				success: function(data) {
					var row = generateRow(data);
		
					$('#myTable tr:last').after(row);
					hidenNewEntry();
				}  
            });
            return false;
		}

		$('#selall').click(function() {
			if ($(".mc").prop('checked')) {
				$(".mc").prop('checked', false);
			} else {
				$(".mc").prop('checked', 'checked');
			}
		});
	
		function create() {
			$("#newEntry").show("slow");
			$("#newEntryButton").show("slow");
		}
		
		function generateRow(entity) {
			
			var row = "<tr id='info' class='"+ entity.id +"'>";
			row += generateCheckbox(entity.id);
			row += generateColumn(entity.id, entity.lastName);
			row += generateColumn(entity.id, entity.firstName);
			row += generateColumn(entity.id, entity.patronymic);
			row += generateColumn(entity.id, entity.mobilePhone);
			row += generateColumn(entity.id, entity.homePhone != null ? entity.homePhone : "");
			row += generateColumn(entity.id, entity.address != null ? entity.address : "");
			row += generateColumn(entity.id, entity.email != null ? entity.email : "");
			row += "</tr>";
			return row;
		}
		
		function generateColumn(onclick, value) {
	    	var column =  "<td>" + value + "</td>";
	    	return column;
		}
		function generateCheckbox(value) {
			var checkboxLine = "<td><input type='checkbox' name='check' value='" + value  + "' id='ch' class='mc'></td>'";
			return checkboxLine;
		}
		
		function deleteRow(id) {
			$("." + id).remove();

		}

		function deleteEntries() {
		
			$("#myForm").submit(function() {
				
				tagsArray = new Array();
				var mc = $(':checkbox[name=check]:checked');
				mc.each(function() {
					tagsArray.push($(this).val());
				});

				$.ajax({
					type: "POST",
					global: false,
					url: "/phonebook/delete",
					cache: false,
					data: "id="+ tagsArray,
					dataType: "json",
					success: function(data1) {
						
					    for (var n in data1) {
					    	deleteRow(data1[n]);
					    }
					}  
	            });
	            return false;
			});
		}

		function validate() {
		
			var field = new Array("lastName", "firstName", 'patronymic', "mobilePhone");//поля обязательные
			
			var error=0; // индекс ошибки
	  
			$("#addNewEntryForm").find(":input").each(function() {// проверяем каждое поле в форме
	            for(var i=0;i<field.length;i++){ // если поле присутствует в списке обязательных
	                if($(this).attr("name")==field[i]){ //проверяем поле формы на пустоту
	                    
	                    if(!$(this).val()){// если в поле пустое
	                        $(this).css('border', 'red 1px solid');// устанавливаем рамку красного цвета
	                        error=1;// определяем индекс ошибки       
	                                                    
	                    } else {
	                        $(this).css('border', 'blue 1px solid');// устанавливаем рамку обычного цвета
	                    }
	                }               
	            }
	       });
	       
	       if(error==0) { // если ошибок нет то отправляем данные
	           return true;
	        } else {
	        	if(error == 1) {
	        		var err_text = "Не все обязательные поля заполнены!";
					$("#messenger").html(err_text); 
					$("#messenger").fadeIn("slow"); 
					return false; //если в форме встретились ошибки , не  позволяем отослать данные на сервер.
				}
	        }
		};

		$("body").on("dblclick", "tr#info", (function($this) {

			var cl = $(this).attr("class");
			
			var tr = $(this);
			
			$.ajax({
				type: "GET",
				url: "/phonebook/edit",
				data: "id="+cl,
				cache: false,
				dataType: "json",
				success: function(data) {
					
					$(tr).after(inputField(data));
					$(tr).addClass("temp");
					$(tr).hide();
					
					$("#newEntry1").show("slow");
					$("#newEntryButton1").show("slow");
					
				}  
            });
			return false;
		}));
		
		function inputField(data) {
			var inputF = '<tr id="newEntry1" id="info">';
			inputF += generateHiddenField(data.id);
			inputF += generateInputField("lastName", data.lastName);
			inputF += generateInputField("firstName", data.firstName);
			inputF += generateInputField("patronymic", data.patronymic);
			inputF += generateInputField("mobilePhone", data.mobilePhone);
			inputF += generateInputField("homePhone", data.homePhone);
			inputF += generateInputField("address", data.address);
			inputF += generateInputField("email", data.email);
			inputF += "</tr>";
			inputF += generateButtons();
			
			return inputF;
		}
		
		function generateInputField(name, value) {
			var inputF = '<td><input type="text" name="'+name+'" value="'+value+'"/></td>';
			return inputF;
		}
		function generateHiddenField(id) {
			var inputF = '<td><input type="hidden" name=id value="'+id+'" /></td>';
			return inputF;
		}
		function generateButtons() {
			var inputF = '<tr id="newEntryButton1">'+
				'<td colspan="8">'+
					'<input type="button" onclick="updateEntry();" value="сохранить"/>'+
					'<input type="button" onclick="cancelNewEntry1();" value="отмена"/>'+
				'</td>'+
			'</tr>';
			return inputF;
		}
		
		function cancelNewEntry1() {
			$("#newEntry1").remove();
			$("#newEntryButton1").remove();
			$("#messenger").hide();
			$(".temp").show();
			$(".temp").removeClass("temp");
			$("#find").hide();
			cancelSearch();
		}
		
		function updateEntry() {
			
			validate();
			
			var map = new Object();
			map["id"] = $("#newEntry1 :input[type=hidden]").val();
			var mc = $("#newEntry1 :input[type=text]");
			mc.each(function() {
				map[$(this).attr("name")] = $(this).val();
			});

			$.ajax({
				type: "POST",
				url: "/phonebook/edit",
				data: "request="+JSON.stringify(map),
				cache: false,
				dataType: "json",
				success: function(data) {
					var row = generateRow(data);
					$(".temp").replaceWith(row);
					cancelNewEntry1();
				}  
            });
            return false;
		}
		
		$("body").on("click", "tr#info", (function($this) {
			cancelNewEntry1();
		}));
		
		/*
		$("body").on("click", "tr:not(#newEntry1)", (function($this) {
			$("tr:not(.temp)").show();
			$(".temp").show();
			$(".temp").removeClass("temp");
			//cancelNewEntry1();
		}));
		*/
		
</script>
</body>
</html>