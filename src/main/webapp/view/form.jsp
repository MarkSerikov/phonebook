<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<div class="edit_form" class="my" > 
	
	<div id="messenger"> </div>
	
	<form id="entryForm" accept-charset="UTF-8">
		<input type="hidden" name="id" value="${entry.id}">
		Фамилия
		<br/>
		<input type="text" name="lastName" value="${entry.lastName}">
		<br/>
		Имя
		<br/>
		<input type="text" name="firstName" value="${entry.firstName}"/>
		<br/>
		Отчество
		<br/>
		<input type="text" name="patronymic" value="${entry.patronymic}" />
		<br/>
		Мобильный телефон
		<br/>
		<input type="text" name="mobilePhone" value="${entry.mobilePhone}" />
		<br/>
		Домашний телефон
		<br/>
		<input type="text" name="homePhone" value="${entry.homePhone}" />
		<br/>
		Адрес
		<br/>
		<input type="text" name="address" value="${entry.address}" />
		<br/>
		E-mail
		<br/>
		<input type="text" name="email" value="${entry.email}" />
		<br/><br/>
		<div class="send">
			<input type="submit" value="save" name="button"> <input type="button" value="отмена" onclick="cancel()">
		</div>
	</form>
</div>
