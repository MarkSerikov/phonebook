<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<link type="text/css" href="view/css/bootstrap.css" rel="Stylesheet" />
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/check.js"></script>
	<style type="text/css">
		#my {
			padding-left: 200px;
			padding-top: 50px;
		}
	</style>
</head>
	<body>
		
		<c:if test="${empty entry}">
			Error: такой записи не существует
			<br/>
			<a href="/phonebook/index">На главную</a>
		</c:if>
		
		<c:if test="${not empty entry}">
			<div id="my">
				<jsp:include page="form.jsp"></jsp:include>
			</div>
		</c:if>

	</body>
</html>
