package com.lardi_trans.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import static org.mockito.Mockito.*;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.action.EntryService;

import junit.framework.TestCase;

public class TestServicePhoneBook extends TestCase {

	@Test
	public void test_save() {

		Entry entry = new Entry();
		entry.setFirstName("Семён");
		entry.setPatronymic("Семёнович");
		entry.setMobilePhone("+987415236");
		entry.setHomePhone("44-22-66");
		entry.setAddress("City");

		EntryDao dao = mock(EntryDao.class);
		when(dao.save(entry)).thenReturn(true);
		
		EntryService entityService = new EntryService();
		entityService.setEntryDao(dao);
		
		boolean flag = entityService.save(entry);
		
		assertFalse(flag);

		entry.setLastName("Семёнов");
		flag = entityService.save(entry);
		
		assertTrue(flag);
		
	}

	@Test
	public void test_update() {

		Entry entry = new Entry();
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");

		EntryDao dao = mock(EntryDao.class);
		when(dao.update(entry)).thenReturn(true);
		
		EntryService entityService = new EntryService();
		entityService.setEntryDao(dao);
		
		boolean flag = entityService.update(entry);
		
		assertFalse(flag);

		entry.setId(12L);

		flag = entityService.update(entry);
		
		assertTrue(flag);

	}

	@Test
	public void get_entry() {
		
		Entry entry = new Entry();

		entry.setId(2L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");

		EntryDao dao = mock(EntryDao.class);
		when(dao.getEntry(2L)).thenReturn(entry);
		
		EntryService entityService = new EntryService();
		entityService.setEntryDao(dao);
		
		Entry entry1 = entityService.getEntry(14L);
		
		assertTrue(entry1 == null);
		
		entry1 = entityService.getEntry(2L);
		
		assertTrue(entry1 != null);
		
		assertEquals("Семёнов", entry1.getLastName());
		assertEquals("Вадим", entry1.getFirstName());
		assertFalse("Семёнович".equals(entry1.getPatronymic()));
		assertEquals("+487478236", entry1.getMobilePhone());
		assertEquals("12-22-66", entry1.getHomePhone());
		
	}
	
	@Test
	public void test_delete() {

		EntryDao dao = mock(EntryDao.class);
		when(dao.delete(13L)).thenReturn(true);
		when(dao.delete(15L)).thenReturn(false);
		
		EntryService entityService = new EntryService();
		entityService.setEntryDao(dao);
		
		boolean flag = entityService.delete(15L);
		
		assertFalse(flag);
		
		flag = entityService.delete(13L);
		
		assertTrue(flag);

	}
	
	@Test
	public void get_all_enties() {

		List<Entry> entries = new LinkedList<Entry>();
		
		EntryDao dao = mock(EntryDao.class);
		when(dao.getAllEntries()).thenReturn(entries);
		
		EntryService entityService = new EntryService();
		entityService.setEntryDao(dao);
		
		List<Entry> listEntries = entityService.getAllEntry();
		
		assertTrue(listEntries.size() == 0);
		
		Entry entry = new Entry();
		Entry entry1 = new Entry();
		
		entry.setId(1L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");
		
		entry1.setId(2L);
		entry1.setLastName("Калымов");
		entry1.setFirstName("Петр");
		entry1.setPatronymic("Генадиевич");
		entry1.setMobilePhone("+25457898");
		entry1.setHomePhone("5-88-77-963");
		entry1.setAddress("West");
		entry1.setEmail("kubic@rubic.net");
		
		entries.add(entry);
		entries.add(entry1);
		
		listEntries = entityService.getAllEntry();
		
		assertTrue(listEntries.size() == 2);
		
	}
}
