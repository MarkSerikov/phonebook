package com.lardi_trans.test.entityService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.action.EntryService;

public class TestEntityServiceSaveMethod {

	static EntryDao dao;
	static EntryService entityService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = mock(EntryDao.class);
		entityService = new EntryService();
		entityService.setEntryDao(dao);
	}

	@Test
	public void test_save_null() {
		
		Entry entry = null;
		
		Long returnId = 3L;
		
		when(dao.save(entry)).thenReturn(returnId);
		
		Long id = entityService.save(entry);
		
		assertFalse(returnId.equals(id));
		
		verify(dao, times(0)).save(entry);

	}
	
	@Test
	public void test_save_entry_without_an_obligatory_fields() {
		
		Entry entry = new Entry();
		entry.setFirstName("Семён");
		entry.setHomePhone("44-22-66");
		entry.setAddress("City");

		Long returnId = 1L;
		
		when(dao.save(entry)).thenReturn(returnId);

		Long id = entityService.save(entry);
		
		verify(dao, times(0)).save(entry);
		
		assertFalse(returnId.equals(id));

		entry.setPatronymic("Семёнович");
		entry.setLastName("Семёнов");
		
		id = entityService.save(entry);
		
		verify(dao, times(0)).save(entry);
		
		assertFalse(returnId.equals(id));
		
	}
	
	@Test
	public void test_save_entry() {
		
		Entry entry = new Entry();
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");
		
		Long id = 1L;
		
		when(dao.save(entry)).thenReturn(id);
		
		Long returnId = entityService.save(entry);
		
		assertTrue(returnId == 1L);
		
		verify(dao).save(entry);
		
	}

}
