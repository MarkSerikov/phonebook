package com.lardi_trans.test.entityService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.action.EntryService;

public class TestEntityServiceGetEntityMethod {

	static EntryDao dao;
	static EntryService entityService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = mock(EntryDao.class);
		entityService = new EntryService();
		entityService.setEntryDao(dao);
	}
	
	@Test
	public void test_get_exists_entry() {
		
		Entry entry = new Entry();

		entry.setId(2L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");

		when(dao.getEntry(2L)).thenReturn(entry);
		
		Entry entry1 = entityService.getEntry(2L);
		
		assertTrue(entry1 != null);
		
		assertEquals("Семёнов", entry1.getLastName());
		assertEquals("Вадим", entry1.getFirstName());
		assertFalse("Семёнович".equals(entry1.getPatronymic()));
		assertEquals("+487478236", entry1.getMobilePhone());
		assertEquals("12-22-66", entry1.getHomePhone());
		
		verify(dao, times(1)).getEntry(2L);
		
	}
	
	@Test
	public void get_not_exists_entry() {

		Entry entry = new Entry();

		entry.setId(33L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");

		
		when(dao.getEntry(33L)).thenReturn(entry);
		when(dao.getEntry(22L)).thenReturn(null);
		
		Entry entry2 = entityService.getEntry(22L);
		
		assertTrue(entry2 == null);
		
		verify(dao, times(1)).getEntry(22L);
		verify(dao, times(0)).getEntry(33L);
	}
}
