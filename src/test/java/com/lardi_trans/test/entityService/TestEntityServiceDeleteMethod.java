package com.lardi_trans.test.entityService;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.service.action.EntryService;

public class TestEntityServiceDeleteMethod {

	static EntryDao dao;
	static EntryService entityService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = mock(EntryDao.class);
		entityService = new EntryService();
		entityService.setEntryDao(dao);
	}

	@Test
	public void test_delete_not_exists_entry() {

		when(dao.delete(15L)).thenReturn(false);

		boolean flag = entityService.delete(15L);

		assertFalse(flag);
		
		verify(dao, times(1)).delete(15L);
		
	}

	@Test
	public void test_delete_exists_entry() {
		
		when(dao.delete(13L)).thenReturn(true);
		
		boolean flag = entityService.delete(13L);
		
		assertTrue(flag);
		
		verify(dao).delete(13L);

	}
}
