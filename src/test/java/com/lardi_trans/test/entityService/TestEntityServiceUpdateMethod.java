package com.lardi_trans.test.entityService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.action.EntryService;

public class TestEntityServiceUpdateMethod {

	static EntryDao dao;
	static EntryService entityService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = mock(EntryDao.class);
		entityService = new EntryService();
		entityService.setEntryDao(dao);
	}

	@Test
	public void test_update_null() {
		
		Entry entry = null;
		
		when(dao.update(entry)).thenReturn(true);
		
		boolean flag = entityService.update(entry);
		
		assertFalse(flag);
		
		verify(dao, times(0)).update(entry);

	}
	
	@Test
	public void test_update_entry_without_an_obligatory_fields() {
		
		Entry entry = new Entry();
		entry.setFirstName("Семён");
		entry.setHomePhone("44-22-66");
		entry.setAddress("City");
		
		when(dao.update(entry)).thenReturn(true);

		boolean flag = entityService.update(entry);
		
		verify(dao, times(0)).update(entry);
		
		assertFalse(flag);

		entry.setPatronymic("Семёнович");
		entry.setLastName("Семёнов");
		
		flag = entityService.update(entry);
		
		verify(dao, times(0)).update(entry);
		
		assertFalse(flag);
		
	}
	
	@Test
	public void test_update_entry() {
		
		Entry entry = new Entry();
		entry.setId(12L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");
		
		when(dao.update(entry)).thenReturn(true);
		
		boolean flag = entityService.update(entry);
		
		assertTrue(flag);
		
		verify(dao).update(entry);
		
	}
}