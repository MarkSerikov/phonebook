package com.lardi_trans.test.entityService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.lardi_trans.test.model.dao.EntryDao;
import com.lardi_trans.test.model.entity.Entry;
import com.lardi_trans.test.service.action.EntryService;

public class TestEntityServiceGetAllEntries {

	static EntryDao dao;
	static EntryService entityService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dao = mock(EntryDao.class);
		entityService = new EntryService();
		entityService.setEntryDao(dao);
	}

	@Test
	public void testGetAllEntry() {

		List<Entry> entries = new LinkedList<Entry>();

		when(dao.getAllEntries()).thenReturn(entries);

		List<Entry> listEntries = entityService.getAllEntry();

		verify(dao, times(1)).getAllEntries();
		
		assertTrue(listEntries.size() == 0);

		Entry entry = new Entry();
		Entry entry1 = new Entry();

		entry.setId(1L);
		entry.setLastName("Семёнов");
		entry.setFirstName("Вадим");
		entry.setPatronymic("Валеревич");
		entry.setMobilePhone("+487478236");
		entry.setHomePhone("12-22-66");
		entry.setAddress("Plaze");
		entry.setEmail("bum@mailic.com");

		entry1.setId(2L);
		entry1.setLastName("Калымов");
		entry1.setFirstName("Петр");
		entry1.setPatronymic("Генадиевич");
		entry1.setMobilePhone("+25457898");
		entry1.setHomePhone("5-88-77-963");
		entry1.setAddress("West");
		entry1.setEmail("kubic@rubic.net");

		entries.add(entry);
		entries.add(entry1);

		listEntries = entityService.getAllEntry();
		
		verify(dao, times(2)).getAllEntries();

		assertTrue(listEntries.size() == 2);

	}

}
